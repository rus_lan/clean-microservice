package main

import (
	"context"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"microservice/internal/app"
	"microservice/internal/repository"
	"microservice/internal/service"
	desc "microservice/pkg"
)

func main() {
	// Getting values from a config
	viper.AddConfigPath("../config")
	viper.SetConfigName("config")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("cannot read from a config: %v", err)
	}
	grpcPort := viper.Get("server.grpc_port").(string)
	httpPort := viper.Get("server.http_port").(string)

	// Launching a database
	db, err := repository.NewDB()
	if err != nil {
		log.Fatalln("cannot init DB: ", err)
	}
	dao := repository.NewDAO(db)

	// Registering all services
	userService := service.NewUserService(dao)

	// Starting grpc server
	go func() {
		listener, err := net.Listen("tcp", grpcPort)
		if err != nil {
			log.Println(err)
		}
		var opts []grpc.ServerOption
		grpcServer := grpc.NewServer(opts...)
		desc.RegisterMicroserviceServer(grpcServer, app.NewMicroservice(userService))

		err = grpcServer.Serve(listener)
		if err != nil {
			log.Fatalln("cannot init GRPC server: ", err)
		}
	}()

	// Starting HTTP server
	var opts []runtime.ServeMuxOption
	mux := runtime.NewServeMux(opts...)
	err = desc.RegisterMicroserviceHandlerServer(context.Background(), mux, app.NewMicroservice(userService))
	if err != nil {
		log.Fatalln("cannot init HTTP server: ", err)
	}
	log.Fatalln(http.ListenAndServe(httpPort, mux))
}
