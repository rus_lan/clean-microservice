package app

import (
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
	desc "microservice/pkg"
)

func (m *Microservice) GetNames(ctx context.Context, empty *emptypb.Empty) (*desc.GetNamesResponse, error) {
	fmt.Println("Hello from a handler level!")
	m.userService.GetNames()
	return nil, nil
}
