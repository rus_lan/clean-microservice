package app

import (
	"microservice/internal/service"
	desc "microservice/pkg"
)

type Microservice struct {
	desc.UnimplementedMicroserviceServer
	userService service.UserService
}

func NewMicroservice(userService service.UserService) *Microservice {
	return &Microservice{
		userService: userService,
	}
}
