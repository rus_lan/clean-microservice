package repository

import "fmt"

type UserQuery interface {
	GetNames()
}

type userQuery struct {
}

func (q *userQuery) GetNames() {
	fmt.Println("Hello from a repository level!")
}
