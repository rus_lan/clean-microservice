package repository

import (
	"database/sql"
	"fmt"

	"github.com/Masterminds/squirrel"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

type DAO interface {
	NewUserQuery() UserQuery
}

var DB *sql.DB

type dao struct {
	DB *sql.DB
}

func NewDB() (*sql.DB, error) {
	host := viper.Get("db.host").(string)
	port := viper.Get("db.port").(int)
	user := viper.Get("db.user").(string)
	dbname := viper.Get("db.dbname").(string)
	password := viper.Get("db.password").(string)

	url := fmt.Sprintf(
		"postgres://%v:%v@%v:%v/%v?sslmode=disable",
		user,
		password,
		host,
		port,
		dbname)

	DB, err := sql.Open("postgres", url)
	if err != nil {
		return nil, err
	}
	return DB, nil
}

func NewDAO(db *sql.DB) DAO {
	DB = db
	return &dao{DB: db}
}

func pgQb() squirrel.StatementBuilderType {
	return squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar).RunWith(DB)
}

func (d *dao) NewUserQuery() UserQuery {
	return &userQuery{}
}
