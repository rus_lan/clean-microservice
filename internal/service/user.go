package service

import (
	"fmt"

	"microservice/internal/repository"
)

type UserService interface {
	GetNames()
}

type userService struct {
	dao repository.DAO
}

func NewUserService(dao repository.DAO) UserService {
	return &userService{dao: dao}
}

func (s *userService) GetNames() {
	fmt.Println("Hello from a service level!")
	s.dao.NewUserQuery().GetNames()
}
